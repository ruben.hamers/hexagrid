﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HexaGrid.Domain.Api;
using HexaGrid.Domain.Entities;
using HexaGrid.Domain.Entities.Layouts;

namespace HexaGrid.Domain.Service
{
    public class GridService : IService
    {
        private IApi _api;

        public GridService(IApi api)
        {
            _api = api;
        }

        public async Task<Grid> GetGrid(CancellationToken cancellationToken)
        {
            if (_api == null)
                return null;
            var template = await _api.GetGridTemplate();
            if (template == null || cancellationToken.IsCancellationRequested)
                return null;
            template.ValidateColors();

            var grid = new Grid(template.TileSize, template.TilePadding, template.DefaultTileColor, template.Tiles?.ToList() ?? new List<Tile>());
            grid.ApplyLayout(GetGridLayout());
            return grid;
        }

        /// <summary>
        /// determine what layout, for now hardcode it to hexagon
        /// </summary>
        /// <returns>Hexagon layout</returns>
        private static IGridLayout GetGridLayout()
        {
            return new HexagonLayout();
        }

        public void Dispose()
        {
        }
    }
}