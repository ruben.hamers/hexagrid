﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HexaGrid.Domain.Service.ServiceProvider
{
    public static class ServiceProvider
    {
        private static Dictionary<Type, IService> _services;

        public static T Get<T>() where T : IService
        {
            if (_services == null)
                return default;
            _services.TryGetValue(typeof(T), out var service);
            return (T)service;
        }

        public static void Init(List<IService> services)
        {
            if (_services != null)
                return;
            _services = services?.ToDictionary(s => s.GetType(), s => s) ?? new Dictionary<Type, IService>();
        }

        public static void Destroy()
        {
            if (_services == null)
                return;
            foreach (var service in _services)
                service.Value.Dispose();

            _services = null;
        }
    }
}