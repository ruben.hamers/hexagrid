﻿using System.Numerics;

namespace HexaGrid.Domain.Entities.Layouts
{
    public interface IGridLayout
    {
        Vector2[] Generate(float tileSize, float tilePadding, int tilesCount);
    }
}