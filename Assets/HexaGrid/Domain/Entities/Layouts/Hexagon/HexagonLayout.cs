﻿using System;
using Vector2 = System.Numerics.Vector2;

namespace HexaGrid.Domain.Entities.Layouts
{
    public class HexagonLayout : IGridLayout
    {
        public Vector2[] Generate(float tileSize, float tilePadding, int tilesCount)
        {
            var layout = new Vector2[tilesCount];
            if (tilesCount == 0)
                return layout;
            // https://mathopenref.com/apothem.html
            float apothem = (float)((tileSize * .5f) * Math.Sqrt(3f) * .5f);

            double _angle, _radius;
            int edges = 6;
            layout[0] = new Vector2();
            int iterations = (tilesCount / edges) + 1;
            var tileIndex = 1;
            for (int iteration = 1; iteration <= iterations && tileIndex < tilesCount; iteration++)
            {
                for (int edge = 0; edge < edges * iteration && tileIndex < tilesCount; edge++)
                {
                    _angle = 2f * Math.PI / (edges * iteration) * edge;
                    _radius = (edge % iteration == 0
                        ? tileSize * iteration
                        : apothem * 2f * iteration)
                              + tilePadding * iteration;
                    layout[tileIndex] = (new Vector2((float)Math.Sin(_angle), (float)Math.Cos(_angle)) * (float)_radius) * .5f;
                    tileIndex++;
                }
            }

            return layout;
        }
    }
}