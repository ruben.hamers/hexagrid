﻿namespace HexaGrid.Domain.Entities
{
    public class GridTemplate
    {
        public float TileSize { get; set; }
        public float TilePadding { get; set; }
        public string DefaultTileColor { get; set; }
        public Tile[] Tiles { get; set; }

        public void ValidateColors()
        {
            DefaultTileColor = ValidateColor(DefaultTileColor);
            foreach (var tile in Tiles)
            {
                if (tile != null)
                    tile.ClickedColor = ValidateColor(tile.ClickedColor);
            }
        }

        private string ValidateColor(string color)
        {
            if (string.IsNullOrEmpty(color))
                return null;
            return !color.StartsWith("#") ? $"#{color}" : color;
        }
    }
}