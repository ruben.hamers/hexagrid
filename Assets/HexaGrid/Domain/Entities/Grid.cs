﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using HexaGrid.Domain.Entities.Layouts;
using HexaGrid.Domain.Extensions;

namespace HexaGrid.Domain.Entities
{
    public class Grid
    {
        public float TileSize { get; private set; }
        public float TilePadding { get; private set; }
        public string DefaultTileColor { get; private set; }
        public IGridLayout Layout { get; private set; }
        public List<Tile> Tiles { get; private set; }
        public int MaxTileCount => Tiles?.LastOrDefault()?.Index + 1 ?? 0;

        public Grid(float tileSize, float tilePadding, string defaultTileColor, List<Tile> tiles)
        {
            TileSize = tileSize;
            TilePadding = tilePadding;
            DefaultTileColor = defaultTileColor;
            Tiles = tiles?.Where(t => t != null).DistinctBy(t => t.Index).OrderBy(t => t.Index).ToList() ?? new List<Tile>();
        }

        public void ApplyLayout(IGridLayout layout)
        {
            Layout = layout;
            if (Tiles == null || Tiles.Count == 0)
                return;
            var positions = Layout?.Generate(TileSize, TilePadding, MaxTileCount) ?? Array.Empty<Vector2>();
            foreach (var tile in Tiles)
                tile.SetPosition(positions[tile.Index]);
        }
    }
}