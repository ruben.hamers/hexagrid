﻿using System.Numerics;

namespace HexaGrid.Domain.Entities
{
    public class Tile
    {
        public int Index { get; set; }
        public string ClickedColor { get; set; }
        public Vector2 Position { get; private set; }

        internal void SetPosition(Vector2 position)
        {
            Position = position;
        }
    }
}