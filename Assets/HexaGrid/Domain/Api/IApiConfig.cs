﻿namespace HexaGrid.Domain.Api
{
    public interface IApiConfig
    {
        public string BaseUrl { get; }
    }
}