﻿using System.Threading.Tasks;
using HexaGrid.Domain.Entities;

namespace HexaGrid.Domain.Api
{
    public interface IApi
    {
        public Task<GridTemplate> GetGridTemplate();
    }
}