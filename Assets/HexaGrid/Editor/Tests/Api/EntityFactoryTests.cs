﻿using HexaGrid.Api.Dto;
using NUnit.Framework;

namespace HexaGrid.Editor.Tests.Api
{
    public class EntityFactoryTests
    {
        [Test]
        public void EntityFactory_DoesNotCrash_WhenGiven_Null_GridTemplateDto()
        {
            Assert.DoesNotThrow(() => { new EntityFactory().CreateGridTemplate(null); });
        }

        [Test]
        public void EntityFactory_DoesNotCrash_WhenGiven_Null_GridTemplate_TilesDto()
        {
            Assert.DoesNotThrow(() => { new EntityFactory().CreateGridTemplate(new GridTemplateDto()); });
        }

        [Test]
        public void EntityFactory_DoesNotCrash_WhenGiven_Null_GridTemplate_TilesDto_Contents()
        {
            Assert.DoesNotThrow(() => { new EntityFactory().CreateGridTemplate(new GridTemplateDto() { Tiles = new[] { new TileDto(), null, new TileDto() } }); });
        }

        [Test]
        public void EntityFactory_Creates_CorrectGridTemplate()
        {
            var entity = new EntityFactory().CreateGridTemplate(new GridTemplateDto()
            {
                DefaultTileColor = "foo",
                TilePadding = 1,
                TileSize = 1,
                Tiles = new[]
                {
                    new TileDto() { Index = 0, ClickedColor = "tile" },
                    null,
                    new TileDto() { Index = 1, ClickedColor = "tile" }
                }
            });

            Assert.AreEqual("foo", entity.DefaultTileColor);
            Assert.AreEqual(1, entity.TilePadding);
            Assert.AreEqual(1, entity.TileSize);
            for (int i = 0; i < entity.Tiles.Length; i++)
            {
                Assert.AreEqual(i, entity.Tiles[i].Index);
                Assert.AreEqual("tile", entity.Tiles[i].ClickedColor);
            }
        }
    }
}