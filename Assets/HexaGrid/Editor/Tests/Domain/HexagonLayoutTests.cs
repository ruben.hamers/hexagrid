﻿using System.Linq;
using System.Numerics;
using HexaGrid.Domain.Entities.Layouts;
using NUnit.Framework;

namespace HexaGrid.Editor.Tests.Domain
{
    public class HexagonLayoutTests
    {
        [Test]
        public void GridLayout_DoesNotCrash_When_AllParams_are_0()
        {
            Assert.DoesNotThrow(() => { new HexagonLayout().Generate(0, 0, 0); });
        }

        [Test]
        public void GridLayout_DoesNotCrash_When_SizeAndPadding_are_0()
        {
            Assert.DoesNotThrow(() => { new HexagonLayout().Generate(0, 0, 1); });
        }

        [Test]
        public void GridLayout_DoesNotCrash_When_Size_is_0()
        {
            Assert.DoesNotThrow(() => { new HexagonLayout().Generate(0, 1, 1); });
        }

        [Test]
        public void GridLayout_DoesNotCrash_When_Padding_is_0()
        {
            Assert.DoesNotThrow(() => { new HexagonLayout().Generate(1, 0, 1); });
        }

        [Test]
        public void When_TilesCount_Is_1_Position_Is_Zero()
        {
            Assert.AreEqual(new Vector2(0, 0), new HexagonLayout().Generate(1, 1, 1).First());
        }

        [Test]
        public void When_TilesCount_Is_2_Position_Of_Index_One_Is_ZeroOne()
        {
            Assert.AreEqual(new Vector2(0, 1), new HexagonLayout().Generate(1, 1, 2).Last());
        }
    }
}