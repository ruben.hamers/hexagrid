﻿using System.Linq;
using HexaGrid.Domain.Extensions;
using NUnit.Framework;

namespace HexaGrid.Editor.Tests.Domain
{
    public class IEnumerableExtensionsTests
    {
        [Test]
        public void DistinctBy_Filters_Out_Duplicates()
        {
            var array = new int[] { 1, 3, 3, 7 };
            var distinct = array.DistinctBy(i => i).ToArray();
            Assert.AreEqual(3, distinct.Length);
            Assert.AreEqual(1, distinct[0]);
            Assert.AreEqual(3, distinct[1]);
            Assert.AreEqual(7, distinct[2]);
        }

        [Test]
        public void DistinctByDoesNothing_When_NoDuplicates()
        {
            var array = new int[] { 1, 2, 3, 4 };
            var distinct = array.DistinctBy(i => i).ToArray();
            Assert.AreEqual(4, distinct.Length);
        }
    }
}