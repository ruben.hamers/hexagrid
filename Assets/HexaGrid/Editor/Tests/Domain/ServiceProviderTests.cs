using System.Collections;
using System.Collections.Generic;
using HexaGrid.Domain.Service;
using HexaGrid.Domain.Service.ServiceProvider;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HexaGrid.Editor.Tests.Domain
{
    public class ServiceProviderTests
    {
        public class FooService : IService
        {
            public void Dispose()
            {
            }
        }

        [SetUp]
        public void Setup()
        {
            ServiceProvider.Destroy();
        }

        [Test]
        public void ServiceProvider_DoesNotCrash_WhenOffered_No_Services()
        {
            Assert.DoesNotThrow(() =>
            {
                ServiceProvider.Init(null);
                ServiceProvider.Get<FooService>();
            });
        }

        [Test]
        public void ServicesCanBeRetrieved_AfterInit()
        {
            ServiceProvider.Init(new List<IService> { new FooService() });
            Assert.NotNull(ServiceProvider.Get<FooService>());
        }

        [Test]
        public void ServicesCan_NOT_BeRetrieved_AfterInit_IfNotAdded()
        {
            ServiceProvider.Init(new List<IService>());
            Assert.Null(ServiceProvider.Get<FooService>());
        }

        [Test]
        public void WhenServiceProvider_IsDestroyed_Services_CanNoLonger_BeRetrieved()
        {
            ServiceProvider.Init(new List<IService> { new FooService() });
            Assert.NotNull(ServiceProvider.Get<FooService>());
            ServiceProvider.Destroy();
            Assert.Null(ServiceProvider.Get<FooService>());
        }

        [TearDown]
        public void TearDown()
        {
            ServiceProvider.Destroy();
        }
    }
}