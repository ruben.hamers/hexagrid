﻿using System.Threading;
using System.Threading.Tasks;
using HexaGrid.Domain.Api;
using HexaGrid.Domain.Entities;
using HexaGrid.Domain.Service;
using NUnit.Framework;

namespace HexaGrid.Editor.Tests.Domain
{
    public class GridServiceTests
    {
        private class MockApi : IApi
        {
            private readonly GridTemplate _template;

            public MockApi(GridTemplate template)
            {
                _template = template;
            }

            public async Task<GridTemplate> GetGridTemplate()
            {
                return _template;
            }
        }

        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void GridService_DoesNotCrash_Api_IsNotSet()
        {
            var gridService = new GridService(null);
            Assert.DoesNotThrow(() => { gridService.GetGrid(new CancellationToken()); });
        }

        [Test]
        public void GridService_DoesNotCrash_WhenNoTemplate_IsReturned()
        {
            var gridService = CreateGridService(null);
            Assert.DoesNotThrow(() => { gridService.GetGrid(new CancellationToken()); });
        }

        [Test]
        public void GridService_DoesNotCrash_Template_WithoutTiles_IsReturned()
        {
            var gridService = CreateGridService(new GridTemplate() { DefaultTileColor = "foo", TilePadding = 1f, TileSize = 1f });
            Assert.DoesNotThrow(() => { gridService.GetGrid(new CancellationToken()); });
        }

        [Test]
        public void GridService_DoesNotCrash_Template_WithEmptyTiles_IsReturned()
        {
            var gridService = CreateGridService(new GridTemplate() { DefaultTileColor = "foo", TilePadding = 1f, TileSize = 1f, Tiles = new[] { new Tile(), null, new Tile() } });
            Assert.DoesNotThrow(() => { gridService.GetGrid(new CancellationToken()); });
        }

        [Test]
        public void GridService_FiltersOut_Duplicate_Indexes_And_empty()
        {
            var gridService = CreateGridService(new GridTemplate() { DefaultTileColor = "foo", TilePadding = 1f, TileSize = 1f, Tiles = new[] { new Tile() { Index = 0 }, null, new Tile() { Index = 0 } } });
            var template = gridService.GetGrid(new CancellationToken()).Result;
            Assert.AreEqual(1, template.Tiles.Count);
        }

        [Test]
        public void GridService_DoesNotCrash_Template_WithNullColors_IsReturned()
        {
            var gridService = CreateGridService(new GridTemplate() { TilePadding = 1f, TileSize = 1f, Tiles = new[] { new Tile(), null, new Tile() } });
            Assert.DoesNotThrow(() => { gridService.GetGrid(new CancellationToken()); });
        }


        [Test]
        public void GridService_Prefixes_GridAndTileColors_With_Hashtag()
        {
            var gridService = CreateGridService(new GridTemplate() { DefaultTileColor = "foo", TilePadding = 1f, TileSize = 1f, Tiles = new[] { new Tile() { Index = 0, ClickedColor = "bar" }, null, new Tile() { Index = 1, ClickedColor = "baz" } } });
            var template = gridService.GetGrid(new CancellationToken()).Result;
            Assert.True(template.DefaultTileColor.StartsWith("#"));
            template.Tiles.ForEach(t => Assert.True(t.ClickedColor.StartsWith("#")));
        }

        private GridService CreateGridService(GridTemplate template)
        {
            return new GridService(new MockApi(template));
        }

        public void TearDown()
        {
        }
    }
}