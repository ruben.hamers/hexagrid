﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using HexaGrid.Domain.Entities;
using HexaGrid.Domain.Entities.Layouts;
using NUnit.Framework;

namespace HexaGrid.Editor.Tests.Domain
{
    public class GridTests
    {
        [Test]
        public void Grid_DoesNotCrash_WithEmptyTiles()
        {
            Assert.DoesNotThrow(() => { new Grid(1, 1, "foo", new List<Tile> { new Tile(), null, new Tile() }); });
        }

        [Test]
        public void Grid_FiltersOut_Duplicate_Indexes_And_empty()
        {
            var grid = new Grid(1, 1, "foo", new List<Tile> { new Tile(), null, new Tile() });
            Assert.AreEqual(1, grid.Tiles.Count);
        }

        [Test]
        public void Grid_MaxIndex_IsBasedOn_MaxTileCount_Instead_Of_ArrayLenght()
        {
            var grid = new Grid(1, 1, "foo", new List<Tile> { new Tile() { Index = 0 }, new Tile() { Index = 12 } });
            Assert.AreEqual(2, grid.Tiles.Count);
            Assert.AreEqual(13, grid.MaxTileCount);
        }

        [Test]
        public void Grid_Generate_Returns_AllVertices_Based_On_MaxTileCount()
        {
            var grid1 = new Grid(1, 1, "foo", new List<Tile> { new Tile() { Index = 0 } });
            grid1.ApplyLayout(new HexagonLayout());
            var grid12 = new Grid(1, 1, "foo", new List<Tile> { new Tile() { Index = 12 } });
            grid12.ApplyLayout(new HexagonLayout());
            Assert.AreEqual(new Vector2(0, 0), grid1.Tiles.First().Position);
            Assert.AreNotEqual(new Vector2(0, 0), grid12.Tiles.First().Position);
        }
    }
}