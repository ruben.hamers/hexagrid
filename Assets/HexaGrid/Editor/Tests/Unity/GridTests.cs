﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using HexaGrid.Domain.Entities.Layouts;
using HexaGrid.Unity.Scripts;
using NUnit.Framework;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.TestTools;
using Grid = HexaGrid.Unity.Scripts.Grid;

namespace HexaGrid.Editor.Tests.Unity
{
    public class GridTests
    {
        private Tile _fakePrefab;
        private Grid _grid;

        [SetUp]
        public void Setup()
        {
            _fakePrefab = new GameObject("Prefab").AddComponent<Tile>();
            _grid = new GameObject("Grid").AddComponent<Grid>();
            typeof(Grid).GetField("_tilePrefab", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(_grid, _fakePrefab);
        }

        [UnityTest]
        public IEnumerator Generate_Returns_Tiles_Based_On_Index_Not_Count()
        {
            var grid = CreateGrid(new List<HexaGrid.Domain.Entities.Tile>()
            {
                new HexaGrid.Domain.Entities.Tile() { ClickedColor = "foo", Index = 0 },
                new HexaGrid.Domain.Entities.Tile() { ClickedColor = "foo", Index = 5 },
            });
            var task = _grid.Generate(grid, new CancellationToken());

            while (!task.IsCompleted || task.Status == TaskStatus.WaitingForActivation)
                yield return null;
            if (task.IsFaulted)
                throw task.Exception;
            Assert.AreEqual(2, _grid.GetComponentsInChildren<Tile>().Length);
        }

        private static HexaGrid.Domain.Entities.Grid CreateGrid(List<HexaGrid.Domain.Entities.Tile> tiles)
        {
            var grid = new HexaGrid.Domain.Entities.Grid(1, 1, "foo", tiles);
            grid.ApplyLayout(new HexagonLayout());
            return grid;
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(_fakePrefab.gameObject);
            _grid.Destroy();
        }
    }
}