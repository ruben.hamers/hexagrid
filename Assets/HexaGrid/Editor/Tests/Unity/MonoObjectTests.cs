﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Timers;
using HexaGrid.Unity.Scripts;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HexaGrid.Editor.Tests.Unity
{
    public class MonoObjectTests
    {
        private class MockMonoObject : AbstractMonoObject
        {
            public void AwakeObject()
            {
                base.Awake();
            }

            public void StartObject()
            {
                base.Start();
            }

            public void EnableObject()
            {
                base.OnEnable();
            }

            public void DisableObject()
            {
                base.OnDisable();
            }

            public void DestroyObject()
            {
                base.OnDestroy();
            }
        }

        private MockMonoObject _mock;
        private const float MAX_TIMEOUT_MILLISECONDS = 2000;

        [SetUp]
        public void Setup()
        {
            _mock = new GameObject().AddComponent<MockMonoObject>();
        }

        [UnityTest]
        public IEnumerator WhenObject_Awakes_The_Awakened_EventIsFired()
        {
            var success = false;

            void AssertTest(AbstractMonoObject mock)
            {
                _mock.Awakened -= AssertTest;
                success = true;
            }

            _mock.Awakened += AssertTest;
            _mock.AwakeObject();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!success && stopwatch.ElapsedMilliseconds < MAX_TIMEOUT_MILLISECONDS)
                yield return null;

            Assert.True(success);
        }

        [UnityTest]
        public IEnumerator WhenObject_Starts_The_Started_EventIsFired()
        {
            var success = false;

            void AssertTest(AbstractMonoObject mock)
            {
                _mock.Started -= AssertTest;
                success = true;
            }

            _mock.Started += AssertTest;
            _mock.StartObject();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!success && stopwatch.ElapsedMilliseconds < MAX_TIMEOUT_MILLISECONDS)
                yield return null;

            Assert.True(success);
        }

        [UnityTest]
        public IEnumerator WhenObject_Enables_The_Enabled_EventIsFired()
        {
            var success = false;

            void AssertTest(AbstractMonoObject mock)
            {
                _mock.Enabled -= AssertTest;
                success = true;
            }

            _mock.Enabled += AssertTest;
            _mock.EnableObject();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!success && stopwatch.ElapsedMilliseconds < MAX_TIMEOUT_MILLISECONDS)
                yield return null;

            Assert.True(success);
        }

        [UnityTest]
        public IEnumerator WhenObject_Disables_The_Disabled_EventIsFired()
        {
            var success = false;

            void AssertTest(AbstractMonoObject mock)
            {
                _mock.Disabled -= AssertTest;
                success = true;
            }

            _mock.Disabled += AssertTest;
            _mock.DisableObject();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!success && stopwatch.ElapsedMilliseconds < MAX_TIMEOUT_MILLISECONDS)
                yield return null;

            Assert.True(success);
        }

        [UnityTest]
        public IEnumerator WhenObject_Destroys_The_Destroyed_EventIsFired()
        {
            var success = false;

            void AssertTest(AbstractMonoObject mock)
            {
                _mock.Destroyed -= AssertTest;
                success = true;
            }

            _mock.Destroyed += AssertTest;
            _mock.DestroyObject();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!success && stopwatch.ElapsedMilliseconds < MAX_TIMEOUT_MILLISECONDS)
                yield return null;

            Assert.True(success);
        }

        [TearDown]
        public void TearDown()
        {
            if (_mock)
                UnityEngine.Object.DestroyImmediate(_mock.gameObject);
            _mock = null;
        }
    }
}