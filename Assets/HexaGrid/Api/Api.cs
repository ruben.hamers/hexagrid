﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HexaGrid.Api.Dto;
using HexaGrid.Domain.Api;
using HexaGrid.Domain.Entities;
using Newtonsoft.Json;

namespace HexaGrid.Api
{
    public class Api : IApi
    {
        private readonly IApiConfig _config;
        private readonly HttpClient _client;
        private readonly EntityFactory _entityFactory;

        public Api(IApiConfig config)
        {
            _config = config;
            _entityFactory = new EntityFactory();
            _client = new HttpClient();
            _client.Timeout = new TimeSpan(0, 0, 15);
        }

        public async Task<GridTemplate> GetGridTemplate()
        {
            try
            {
                var response = await _client.GetAsync(GetEndpoint("public", "dev-cases", "hexagonGrid.json"));
                GridTemplate gridTemplate = null;
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    var template = JsonConvert.DeserializeObject<GridTemplateDto>(data);
                    gridTemplate = _entityFactory.CreateGridTemplate(template);
                }

                return gridTemplate;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private string GetEndpoint(params string[] endpoint)
        {
            if (_config == null)
                return string.Empty;
            var url = Path.Combine(new[] { _config.BaseUrl }.Concat(endpoint).ToArray()).Replace('\\', '/');
            return url;
        }
    }
}