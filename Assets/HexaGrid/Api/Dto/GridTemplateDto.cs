﻿namespace HexaGrid.Api.Dto
{
    public class GridTemplateDto
    {
        public float TileSize { get; set; }
        public float TilePadding { get; set; }
        public string DefaultTileColor { get; set; }
        public TileDto[] Tiles { get; set; }
    }
}