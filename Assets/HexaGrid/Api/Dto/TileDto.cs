﻿namespace HexaGrid.Api.Dto
{
    public class TileDto
    {
        public int Index { get; set; }
        public string ClickedColor { get; set; }
    }
}