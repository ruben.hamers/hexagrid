﻿using System;
using System.Linq;
using HexaGrid.Domain.Entities;

namespace HexaGrid.Api.Dto
{
    public class EntityFactory
    {
        public GridTemplate CreateGridTemplate(GridTemplateDto dto)
        {
            if (dto == null)
                return null;

            return new GridTemplate()
            {
                TileSize = dto.TileSize,
                TilePadding = dto.TilePadding,
                DefaultTileColor = dto.DefaultTileColor,
                Tiles = dto.Tiles?.Where(t => t != null).Select(CreateTile).ToArray() ?? Array.Empty<Tile>()
            };
        }

        private Tile CreateTile(TileDto dto)
        {
            if (dto == null)
                return null;
            return new Tile
            {
                Index = dto.Index,
                ClickedColor = dto.ClickedColor
            };
        }
    }
}