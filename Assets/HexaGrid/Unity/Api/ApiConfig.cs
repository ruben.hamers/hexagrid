﻿using HexaGrid.Domain.Api;
using UnityEngine;

namespace HexaGrid.Unity.Api
{
    [CreateAssetMenu(fileName = "ApiConfig", menuName = "Dephion/ApiConfig", order = 1)]
    public class ApiConfig : ScriptableObject, IApiConfig
    {
        public string BaseUrl => _baseUrl;
        [SerializeField] private string _baseUrl;
    }
}