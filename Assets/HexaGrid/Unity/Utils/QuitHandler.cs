﻿using System;
using HexaGrid.Unity.Scripts;
using UnityEngine;

namespace HexaGrid.Unity.Utils
{
    public class QuitHandler : AbstractMonoObject
    {
        private Action _quitCallback;

        public static void Spawn(Action callback)
        {
            QuitHandler qh = new GameObject("QuitHandler").AddComponent<QuitHandler>();
            qh._quitCallback = callback;
            qh.hideFlags = HideFlags.HideInHierarchy;
        }

        private void OnApplicationQuit()
        {
            _quitCallback.Invoke();
        }
    }
}