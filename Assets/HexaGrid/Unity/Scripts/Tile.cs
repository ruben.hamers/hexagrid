﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace HexaGrid.Unity.Scripts
{
    public class Tile : AbstractMonoObject
    {
        public event Action<Tile> Clicked;
        private List<Renderer> _renderers;
        private Color _defaultColor, _clickedColor;
        private bool _isExpanded;
        private List<TweenerCore<Color, Color, ColorOptions>> _colorTweens;
        private TweenerCore<Vector3, Vector3, VectorOptions> _moveTween;
        private TweenerCore<Vector3, Vector3, VectorOptions> _scaleTween;

        public void Init(Vector3 scale, Color defaultTileColor, Color clickedColor)
        {
            _renderers = GetComponentsInChildren<Renderer>().ToList();
            _scaleTween = transform.DOScale(scale, .2f);
            _defaultColor = defaultTileColor;
            _clickedColor = clickedColor;
            ForceColor(defaultTileColor);
        }

        public override void Destroy()
        {
            KillAllTweens();
            base.Destroy();
        }

        public void ForceColor(Color color)
        {
            _renderers.ForEach(r => r.material.color = color);
        }

        public void Animate(float duration)
        {
            if (_isExpanded)
                Collapse(duration);
            else
                Expand(duration);
        }

        private void Expand(float duration)
        {
            DoTween(_clickedColor, 0.2f, duration, () => { _isExpanded = true; });
        }

        private void Collapse(float duration)
        {
            DoTween(_defaultColor, 0, duration, () => { _isExpanded = false; });
        }

        private void DoTween(Color color, float height, float duration, Action callback)
        {
            KillAllTweens();
            _colorTweens = _renderers.SelectMany(r => r.materials.Select(m => m.DOColor(color, duration))).ToList();
            _moveTween = transform.DOLocalMoveY(height, duration).OnComplete(() => { callback?.Invoke(); });
        }

        private void KillAllTweens()
        {
            if (_colorTweens != null)
                _colorTweens.ForEach(ct => ct.Kill());
            if (_moveTween != null)
            {
                if (!_moveTween.IsComplete())
                    _isExpanded = !_isExpanded;
                _moveTween.Kill();
            }

            _scaleTween?.Kill();
        }

        private void OnMouseDown()
        {
            OnClicked();
        }

        private void OnClicked()
        {
            Clicked?.Invoke(this);
        }

        public void Return(float duration)
        {
            Collapse(duration);
        }
    }
}