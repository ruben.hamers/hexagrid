﻿using System.Collections.Generic;
using HexaGrid.Domain.Service;
using HexaGrid.Domain.Service.ServiceProvider;
using HexaGrid.Unity.Api;
using HexaGrid.Unity.Utils;
using UnityEngine;

namespace HexaGrid.Unity.Scripts
{
    public class Main
    {
        private static bool _initialized;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void SceneStarted()
        {
            if (_initialized)
                return;
            _initialized = true;
            InitServiceProvider();
            QuitHandler.Spawn(Destroy);
        }

        private static void InitServiceProvider()
        {
            ServiceProvider.Init(GatherServiceDependencies());
        }

        /// <summary>
        /// This stuff might be in some scriptable object in the future, to expose it to designers in Unity.
        /// But for now, let's do it here in memory
        /// </summary>
        /// <returns></returns>
        private static List<IService> GatherServiceDependencies()
        {
            var apiConfig = Resources.Load<ApiConfig>("Dephion/ApiConfig");
            if (!apiConfig)
                Debug.LogError("ApiConfig Could not be found, please make sure you generate an ApiConfig ScriptableObject! (At path Resources/Dephion)");
            return new List<IService>
            {
                new GridService(new HexaGrid.Api.Api(apiConfig))
            };
        }

        private static void Destroy()
        {
            ServiceProvider.Destroy();
            _initialized = false;
        }
    }
}