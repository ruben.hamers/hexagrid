﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace HexaGrid.Unity.Scripts
{
    public class Grid : AbstractMonoObject
    {
        [SerializeField] private Tile _tilePrefab;
        private List<Tile> _tiles = new List<Tile>();
        private Color _defaultTileColor;
        private Tile _activeTile;
        private const float DURATION = .5f;

        public override void Destroy()
        {
            DestroyTiles();
            base.Destroy();
        }

        private void DestroyTiles()
        {
            if (_tiles.Count > 0)
                _tiles.ForEach(t =>
                {
                    if (t)
                        t.Destroy();
                });
        }

        public async Task Generate(Domain.Entities.Grid grid, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
                return;
            if (_tilePrefab == null || grid == null)
            {
                Debug.LogWarning("Tile prefab is not set or the Grid is null! Cannot generate a grid!");
                return;
            }

            DestroyTiles();
            if (!TryConvertColor(grid.DefaultTileColor, out _defaultTileColor))
                Debug.LogWarning($"Failed to convert the default tile color to a Unity Color: {grid.DefaultTileColor}, it will be set to white");

            foreach (var tile in grid.Tiles)
            {
                if (cancellationToken.IsCancellationRequested)
                    break;
                _tiles.Add(CreateTile(grid.TileSize, tile, cancellationToken));
                // ReSharper disable once MethodSupportsCancellation <-- will throw an exception and I'll try to avoid it
                await Task.Delay(100);
            }
        }

        private Tile CreateTile(float size, Domain.Entities.Tile currentTile, CancellationToken tokenSource)
        {
            if (tokenSource.IsCancellationRequested)
                return null;
            var tile = Instantiate(_tilePrefab, new Vector3(currentTile.Position.X, 0, currentTile.Position.Y), Quaternion.identity, transform);
            tile.transform.localScale = Vector3.zero;
            TryConvertColor(currentTile.ClickedColor, out var color);
            tile.Init(new Vector3(size, size, size), _defaultTileColor, color);
            tile.Clicked += TileOnClicked_Handler;
            tile.Destroyed += TileOnDestroyed_Handler;
            return tile;
        }

        private void TileOnClicked_Handler(Tile tile)
        {
            if (tile == _activeTile)
            {
                tile.Return(.5f);
                _activeTile = null;
                return;
            }

            if (_activeTile)
                _activeTile.Return(DURATION);
            tile.Animate(DURATION);
            _activeTile = tile;
        }

        private void TileOnDestroyed_Handler(AbstractMonoObject monoObject)
        {
            var tile = monoObject as Tile;
            tile.Clicked -= TileOnClicked_Handler;
            tile.Destroyed -= TileOnDestroyed_Handler;
        }

        private bool TryConvertColor(string colorString, out Color color)
        {
            var success = ColorUtility.TryParseHtmlString(colorString, out color);
            if (!success)
                color = Color.white;

            return success;
        }
    }
}