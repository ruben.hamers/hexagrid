using System.Threading;
using HexaGrid.Domain.Service;
using HexaGrid.Domain.Service.ServiceProvider;
using UnityEngine;
using UnityEngine.UI;

namespace HexaGrid.Unity.Scripts
{
    public class GridView : AbstractMonoObject
    {
        [SerializeField] private Grid _gridPrefab;
        private Button _button;
        private Grid _spawnedGrid;
        private CancellationTokenSource _tokenSource;

        protected override void Awake()
        {
            base.Awake();
            SetupButton();
        }

        private void SetupButton()
        {
            _button = GetComponentInChildren<Button>();
            if (_button)
                _button.onClick.AddListener(GenerateGrid);
            else
                Debug.LogError("No button found in the gridview!");
        }

        private async void GenerateGrid()
        {
            _tokenSource?.Cancel();
            _tokenSource = new CancellationTokenSource();
            if (_gridPrefab == null)
            {
                Debug.LogError("Cannot generate the grid, no Grid prefab assigned in the grid view");
                return;
            }

            DestroyGrid();
            var gridService = ServiceProvider.Get<GridService>();
            var grid = await gridService.GetGrid(_tokenSource.Token);
            if (_tokenSource.Token.IsCancellationRequested)
                return;
            _spawnedGrid = Instantiate(_gridPrefab);
            await _spawnedGrid.Generate(grid, _tokenSource.Token);
            if (_tokenSource.Token.IsCancellationRequested)
                DestroyGrid();
        }

        private void DestroyGrid()
        {
            if (_spawnedGrid)
                _spawnedGrid.Destroy();
            _spawnedGrid = null;
        }
    }
}