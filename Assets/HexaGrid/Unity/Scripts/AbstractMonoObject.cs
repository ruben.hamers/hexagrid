﻿using System;
using UnityEngine;

namespace HexaGrid.Unity.Scripts
{
    public class AbstractMonoObject : MonoBehaviour
    {
        public event Action<AbstractMonoObject> Awakened, Started, Enabled, Disabled, Destroyed;

        protected virtual void Awake()
        {
            OnAwaken();
        }

        protected virtual void Start()
        {
            OnStarted();
        }

        protected virtual void OnEnable()
        {
            OnEnabled();
        }

        protected virtual void OnDisable()
        {
            OnDisabled();
        }

        protected virtual void OnDestroy()
        {
            OnDestroyed();
        }

        public virtual void Destroy()
        {
            if (Application.isPlaying)
                Destroy(gameObject);
            else
                DestroyImmediate(gameObject);
        }

        private void OnAwaken()
        {
            Awakened?.Invoke(this);
        }

        private void OnStarted()
        {
            Started?.Invoke(this);
        }

        private void OnEnabled()
        {
            Enabled?.Invoke(this);
        }

        private void OnDisabled()
        {
            Disabled?.Invoke(this);
        }

        private void OnDestroyed()
        {
            Destroyed?.Invoke(this);
        }
    }
}