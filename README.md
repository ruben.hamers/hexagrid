# Hexagon Grid example project
This is a simple example of how to solve the Dephion take-home assignment.
It is not the only solution, but rather one way of approaching the problem.
I have gone to great lengths to separate all the modules from each other.
This might not be realistic (and slightly over-engineered) but it should give applicants (you) an idea of how to separate concerns in an application. 

## Assignments
There are two sub-assignments in this case.
First we need to create a design document for implementing a 3D hexagon grid.
See [this documentation](Documentation~/Assignment/Unity%20dev%20tech%20assignment.pdf) for the case description.
Next we need to implement our design and if needed, adapt the document to reflect the implementation.

Now, Let's go have some fun and start with a simple design!

## Technical design
The technical design document can be found [here](Documentation~/Technical%20Design.docx). Please note that the documentation is written in an explanatory tone.
I did this as a means of communication to applicants might they ever ask for an example implementation.

## Implementation
The application can be found in the folder called [HexaGrid](Assets/HexaGrid).
Here you will find the folders that correspond with the bounded contexts (domains) as described in the design document (including a folder called Editor where the unit tests reside, more about this later). 

Let's check them out:

### Api
This folder contains the concrete implementation of our [API](Assets/HexaGrid/Api/Api.cs). We only have a single endpoint to worry about now, but there might be more in the future.
It also contains a folder called [DTO](Assets/HexaGrid/Api/Dto) which stands for ``Data Transfer Objects``. These objects are simple data structures that are send across the wire and should not contain any logic.
I also added an [EntityFactory](Assets/HexaGrid/Api/Dto/EntityFactory.cs) to separate the concern of object creation and usage (which can also be injected as a dependency later).

### Domain
The domain layer is the hearth of the application. Here is where all business logic for our grid resides.
It defines the [interface for the API](Assets/HexaGrid/Domain/Api/IApi.cs) we want to use (which was implemented in the Api context).
Next we have some layouts, which only contains the [hexagon layout](Assets/HexaGrid/Domain/Entities/Layouts/Hexagon/HexagonLayout.cs) for now. We can add more layouts and simply apply them to the grid.
Then we have our entities for the [Grid](Assets/HexaGrid/Domain/Entities/Grid.cs) and [Tile](Assets/HexaGrid/Domain/Entities/Tile.cs). Next a simple [extension for an IEnumerable](Assets/HexaGrid/Domain/Extensions/IEnumerableExtensions.cs).

Last we have a folder called Service. In many Unity projects there are things called controllers or managers that act as singletons. 
In out case, we have one static class called [ServiceProvider](Assets/HexaGrid/Domain/Service/ServiceProvider/ServiceProvider.cs) that is populated when Unity starts (more about this later).
In here we also keep a service called the [GridService](Assets/HexaGrid/Domain/Service/GridService.cs) which offers an api to get a grid, and convert it to the correct business entity we need.

### Unity
The Unity domain is essentially our outer most layer and thus, it is dependent on many parts of the application.
First we have a folder called API, which defines the implementation of our [ApiConfig](Assets/HexaGrid/Unity/Api/ApiConfig.cs) which in this case is a Scriptable Object.
The [SO](Assets/HexaGrid/Unity/Resources/Dephion/ApiConfig.asset) resides in the Resources folder so we can access it at any time.
Then there is a folder called external that contains the DoTween library, a models folder with the hexagon model, a prefabs directory with the prefabs and Utils folder with a simple object that catches the OnApplicationQuit event from Unity.

The most important folder in here is the scripts folder which contains an [abstract class for Monobehaviours](Assets/HexaGrid/Unity/Scripts/AbstractMonoObject.cs) that raises events at aware, start enable, disable and destroy.
This is something I usually add to a Unity Project if it isn't there since it is very useful to have.
Next we have the [Grid MonoBehaviour](Assets/HexaGrid/Unity/Scripts/Grid.cs) which is used to generate the grid in 3D. It simply offers a Generate function that can be called given a Grid entity object.
This Monobehaviour grid is a very simple object since the business logic is encapsulated in the Grid Entity. 
The [GridView](Assets/HexaGrid/Unity/Scripts/GridView.cs) is just a wrapper to be able to couple the button to the GridService and spawn the grid. This can be changed to any other class or triggering mechanism in the future.
The [Tile](Assets/HexaGrid/Unity/Scripts/Tile.cs) is the Monobehaviour for the individual hexagon tiles in the scene. It simply registers clicks and makes sure the clicked event is fired, and also contains the logic for colouring and moving itself.

Last, one of the most important classes in this application is the [Main](Assets/HexaGrid/Unity/Scripts/Main.cs) class.
Since Unity does not offer an out of the box manner for Dependency Injection (yes, yes I know there are workarounds with libraries etc.) I often write a class like Main. 
As in the good old console applications you normally write. This Main class will setup and spread dependencies around in the application.
I setup a very basic version of this. I simply load the ApiConfig and initialize an Api and GridService and configure the dependencies.

## Unit Tests
As a repeatable proof that the code actually works I added numerous unit tests. These cover close to everything that is interesting for our solution here.
There are infinite more tests to write, and yes, the generation algorithm itself is not covered by the unit tests since I could not find a nice way to cover it.
If you know a way to do this properly, let me know ;) 
You can run the tests from the Unity Test runner window.

## CI/CD
As with all modern projects I setup a CI/CD pipeline. It simply runs the tests and builds the app for windows.
Very easy to setup nowadays with the CI example project hosted [here](https://gitlab.com/gableroux/unity3d-gitlab-ci-example/)